import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfessionalContactComponent } from './professional-contact.component';

describe('ProfessionalContactComponent', () => {
  let component: ProfessionalContactComponent;
  let fixture: ComponentFixture<ProfessionalContactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfessionalContactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfessionalContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
