import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LangService } from '../services/lang.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  translate: TranslateService;

  constructor(public lang: LangService, private router: Router) {
    this.translate = lang.getTranslateService();
  }

  ngOnInit(): void { }

  getAway() {
    window.location.replace('https://www.google.com/search?tbm=isch&source=hp&biw=1920&bih=907&ei=lzqJXrHgJ8aflwTNlJQI&q=kittens&oq=kittens&gs_lcp=CgNpbWcQAzICCAAyAggAMgIIADICCAAyAggAMgIIADICCAAyAggAMgIIADICCABKGAgXEhQwZzcxZzczZzgxZzc2Zzc2ZzEwN0oRCBgSDTBnMWcxZzFnMWcxZzFQlQtY7BBglRFoAHAAeACAAWmIAdQDkgEDNS4xmAEAoAEBqgELZ3dzLXdpei1pbWc&sclient=img&ved=0ahUKEwjxh7KSl9DoAhXGz4UKHU0KBQEQ4dUDCAc&uact=5');
  }
}
