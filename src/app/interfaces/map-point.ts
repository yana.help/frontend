export interface MapPoint {
  name: string;
  address: string;
  postalCode: string;
  city: string;
  canton: string;
  description: string;
  tel: string;
  email: string;
  type: string;
  website: string;
  longitude: string;
  latitude: string;
}
