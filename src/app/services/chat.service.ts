import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { firestore } from 'firebase/app';
import { map, switchMap } from 'rxjs/operators';
import { Observable, combineLatest, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor(
    private afs: AngularFirestore,
    private router: Router,    
  ) { }

  get(chatId) {
    return this.afs
      .collection<any>('chats')
      .doc(chatId)
      .snapshotChanges()
      .pipe(
        map( doc => {
          // FIXME: ça ne devrait pas être nécessaire de cast la list retournée...
          return { id: doc.payload.id, ...doc.payload.data() as {}}
        })
      );
  }
  getAll(){
    return this.afs
      .collection<any>('chats')
      .snapshotChanges();
  }
}
