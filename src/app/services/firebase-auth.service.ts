import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class FirebaseAuthService {
  createNewUser(email: string, password: string) {
    return new Promise(
      (resolve, reject) => {
        this.firebaseAuth.createUserWithEmailAndPassword(email, password).then(
          () => {
            resolve();
          },
          (error) => {
            reject(error);
          }
        );
      }
    );
  }

  signInUser(email: string, password: string) {
    return new Promise(
      (resolve, reject) => {
        this.firebaseAuth.signInWithEmailAndPassword(email, password).then(
          () => {
            resolve();
          },
          (error) => {
            reject(error);
          }
        );
      }
    );
  }

  signOutUser() {
    this.firebaseAuth.signOut();
  }

  getUser(): Promise<firebase.User> {
    return this.firebaseAuth.currentUser;
  }

  constructor(private firebaseAuth: AngularFireAuth) { }
}
