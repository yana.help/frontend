import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { MapPoint } from '../interfaces/map-point';
import * as sampleData from '../../assets/maps/cantons.json';

@Injectable({
  providedIn: 'root'
})
export class FirebaseDataService  {
  constructor(protected firebaseDB: AngularFireDatabase) { }
  regex: RegExp = /\(([^()]*)\)/g;

  getCantons(): string[] {
    return (sampleData as any).default;
  }

  async getCantonData(canton: string): Promise<MapPoint[]> {
    let results: MapPoint[] = [];
    await new Promise((resolve, reject) =>
      this.firebaseDB.list('/data/safe-spaces/').valueChanges().subscribe((v: MapPoint[]) =>
        (v.length > 0) ? resolve(v) : reject({ error: 'No results '})
      )
    ).then((v: MapPoint[]) =>
      results = v.filter((mp: MapPoint) => mp.canton === canton)
    ).catch((reason) =>
      console.log(reason)
    );

    return results;
  }

  async updatePoint(point: MapPoint) {
    const ref = this.firebaseDB.database.ref('data/safe-spaces');
    ref.orderByChild('name')
      .equalTo(point.name)
      .orderByChild('address')
      .equalTo(point.address)
      .orderByChild('postalCode')
      .equalTo(point.postalCode)
      .once('value', (snapshot: firebase.database.DataSnapshot) => {
        snapshot.forEach((s) => {
          s.ref.update({ latitude: parseFloat(point.latitude), longitude: parseFloat(point.longitude) });
        });
    }
    );
  }
}
