import { Injectable } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Injectable({
  /**
   * @deprecated there should be no neeed for a wrapper.
   */
  providedIn: 'root'
})

export class LangService {
  browserLang: string;

  setLangToken(value: string) {
    this.translate.use(value);
    this.browserLang = value;
    localStorage.setItem('lang', value);
  }

  /**
   * @deprecated there should be no neeed for a wrapper.
   */
  getTranslateService(): TranslateService {
    return this.translate;
  }

  /**
   * @usage
   * ```
   * <div>
   *   <img translate [src]="lang.loadImage( 'FILES.TEST' | translate )">
   * </div>
   * ```
   */
  loadImage(url: string): string {
    return `assets/images/${this.browserLang}/${url}`;
  }

  constructor(public translate: TranslateService) {
    translate.addLangs(['en', 'fr', 'de', 'it']);
    translate.setDefaultLang('en');

    const storedLang = localStorage.getItem('lang');
    const browserLang = storedLang === null ? translate.getBrowserLang() : storedLang;
    this.browserLang = browserLang;
    translate.use(browserLang.match(/en|fr|de|it/) ? browserLang : 'en');
  }
}


