import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { HostListener } from "@angular/core";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'versusvirus2020';

  public isMobile: boolean;
  public mobileMenuActive: boolean = false;

  constructor(private router: Router,) {}

  ngOnInit(): void { 
  }

}
