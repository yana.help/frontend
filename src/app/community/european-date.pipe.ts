import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
import { LangService } from '../services/lang.service';

@Pipe({
  name: 'europeanDate'
})
export class EuropeanDatePipe implements PipeTransform {
  constructor(private lang: LangService) { }

  transform(value: any, pattern: string = 'yyyy-MM-dd'): any {
    const datePipe: DatePipe = new DatePipe(this.lang.browserLang);
    return datePipe.transform(value, pattern);
  }

}
