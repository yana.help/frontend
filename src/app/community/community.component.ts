import { Component, OnInit } from '@angular/core';
import * as jsonData from '../../assets/messages.json';
import { ChatService } from '../services/chat.service.js';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-community',
  templateUrl: './community.component.html',
  styleUrls: ['./community.component.css']
})
export class CommunityComponent implements OnInit {
  chat: Observable<any>
  users: string[];
  messages: any[] = (jsonData as any).default;
  constructor(
    public chatService: ChatService, 
    private router: ActivatedRoute) {
    this.users = this.messages.map(data => {
      // console.log(data.name);
      return data.name;
    });
  }

  ngOnInit(): void {
    console.log(this.users );
    if(this.router.snapshot.paramMap.has('id')){
      const chatId = this.router.snapshot.paramMap.get('id');
      this.chat = this.chatService.get(chatId);
    } else {
      this.chatService.getAll().subscribe(res => {
        console.log('Les résultats de la query firebase');
        for (let rec of res) {
          console.log(rec.payload.doc.data())
        }
      })
    }
  }

}
