import { Component, OnInit } from '@angular/core';
import * as sampleData from '../../assets/data.json';
import { LangService } from '../services/lang.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-preventive-measures',
  templateUrl: './preventive-measures.component.html',
  styleUrls: ['./preventive-measures.component.css']
})
export class PreventiveMeasuresComponent implements OnInit {
  Texts = (sampleData as any).default;
  translate: TranslateService;

  constructor(public lang: LangService) {
    this.translate = lang.getTranslateService();
  }

  ngOnInit(): void {
  }

  truncateText(selector) {
    const maxLength = 200;
    if (selector.length > maxLength) {
      selector = selector.substr(0, maxLength) + '...';
    }
    return selector;
  }
}
