import { Component, OnInit, ViewChild, ElementRef, AfterViewChecked } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LangService } from '../services/lang.service';
import { Router } from '@angular/router';
import { ChangeDetectorRef } from '@angular/core';
import { fromEvent, Observable, Subscription } from "rxjs";
import { debounceTime } from 'rxjs/operators';

/**
 * The main navigation bar of the website animated such that a line is always following the selected component
 */
@Component({
  selector: 'app-horizontal-header',
  templateUrl: './horizontal-header.component.html',
  styleUrls: ['./horizontal-header.component.css'],
})
export class HorizontalHeaderComponent implements OnInit, AfterViewChecked {
  @ViewChild('GS', {read: ElementRef})
  GS: ElementRef;

  @ViewChild('maps', {read: ElementRef})
  maps: ElementRef;

  @ViewChild('community', {read: ElementRef})
  community: ElementRef;

  @ViewChild('logIn', {read: ElementRef})
  logIn: ElementRef;

  private resizeObservable$: Observable<Event>;

  /**
   * The translation service accessible through the whole app.
   *
   * @deprecated This service should be injected like any other.
   * TODO: changer l'injection de ce service.
   */
  translate: TranslateService;
  position: string = '0';
  width: number = 0;
  /**
   * La taille d'un élément
   * @deprecated prfer a generic private method
   */
  GSWidth: number;
  /**
   * La taille d'un élément
   * @deprecated prfer a generic private method
   */
  mapsWidth: number;
  /**
   * La taille d'un élément
   * @deprecated prfer a generic private method
   */
  communityWidth: number;
  /**
   * La taille d'un élément
   * @deprecated prfer a generic private method
   */
  logInWidth: number;
  private selectedOption: number;

  /**
   * @constructor HorizontalHeaderComponent constuctor which injects necessary services and initializes the
   * @deprecated language service property
   * @param lang @deprecated the wrapper for the language service should not be necessary anymore
   * @param router the standard angular router service which is necessary for navigation
   * @param cdRef the angular change detection service
   */
  constructor(public lang: LangService, private router: Router, private cdRef: ChangeDetectorRef) {
    this.translate = lang.getTranslateService();
  }

  ngOnInit(): void {
    /**
     * Detection of window resize event and call to update the highlight line with a small delay
     * to get accurate values
     */
    this.resizeObservable$ = fromEvent(window, 'resize');
    this.resizeObservable$.pipe(debounceTime(300)).subscribe( evt => {
      this.updateValues();
    });
  }

  ngAfterViewChecked(): void {
    this.updateValues();
  }

  /**
   * Method used to update the different widths of the text options
   */
  updateValues(): void {
    const updateCheck = this.GSWidth;

    this.GSWidth = this.GS.nativeElement.offsetWidth;
    this.mapsWidth = this.maps.nativeElement.offsetWidth;
    this.communityWidth = this.community.nativeElement.offsetWidth;
    this.logInWidth = this.logIn.nativeElement.offsetWidth;

    if(updateCheck == 0 && this.GSWidth != updateCheck) this.toggleState(1); // highlight by default 'Getting started' when the component is initialized
    else this.toggleState(this.selectedOption); // otherwise update the highlight line for the current navigation option selected

    this.cdRef.detectChanges();
  }

  /**
   * Method used to exit the page and navigate to a google image search of cats
   */
  getAway() {
    window.location.replace('https://www.google.com/search?tbm=isch&source=hp&biw=1920&bih=907&ei=lzqJXrHgJ8aflwTNlJQI&q=kittens&oq=kittens&gs_lcp=CgNpbWcQAzICCAAyAggAMgIIADICCAAyAggAMgIIADICCAAyAggAMgIIADICCABKGAgXEhQwZzcxZzczZzgxZzc2Zzc2ZzEwN0oRCBgSDTBnMWcxZzFnMWcxZzFQlQtY7BBglRFoAHAAeACAAWmIAdQDkgEDNS4xmAEAoAEBqgELZ3dzLXdpei1pbWc&sclient=img&ved=0ahUKEwjxh7KSl9DoAhXGz4UKHU0KBQEQ4dUDCAc&uact=5');
  }

  /**
   * Method used to modify the width and position of the highlight line
   * 
   * @param {number} menuOption the navigation option to highlight
   */
  toggleState(menuOption: number) {
    this.selectedOption = menuOption;
    switch (menuOption) {
      case 1:
        this.position = this.GS.nativeElement.getBoundingClientRect().left + 'px';
        this.width = this.GSWidth;
        break;
      case 2:
        this.position = this.maps.nativeElement.getBoundingClientRect().left + 'px';
        this.width = this.mapsWidth;
        break;
      case 3:
        this.position = this.community.nativeElement.getBoundingClientRect().left + 'px';
        this.width = this.communityWidth;
        break;
      case 4:
        this.position = this.logIn.nativeElement.getBoundingClientRect().left + 'px';
        this.width = this.logInWidth;
        break;
      default:
        break;
    }
  }
}
