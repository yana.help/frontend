import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { MapsAPILoader } from '@agm/core';
import { GeocodeService } from '../services/geocode.service';
import { Location } from '../services/location-representation';
import { LangService } from '../services/lang.service';
import { TranslateService } from '@ngx-translate/core';
import { FirebaseDataService } from '../services/firebase-data.service';
import { MapPoint } from '../interfaces/map-point';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.css']
})
export class MapsComponent implements OnInit {
  location: Location;
  centersFound = false;
  geocoder: any;
  cantonCenter: Location = { lat: 46.9479222, lng: 7.4446085 };
  translate: TranslateService;
  private sub: any;

  typeMap: {};

  cantonSelected = null;
  cantons = this.firebaseDB.getCantons();

  cantonData: MapPoint[] = [];
  regex: RegExp = /\(([^()]*)\)/g;

  constructor(public mapsAPI: MapsAPILoader,
              private geocodeService: GeocodeService,
              private ref: ChangeDetectorRef,
              public lang: LangService,
              private firebaseDB: FirebaseDataService,
              private route: ActivatedRoute) {
    this.translate = lang.getTranslateService();
    this.mapsAPI.load().then(() => { });
  }

  getLocation(address: string, cp: string, city: string, isCanton: boolean) {
    this.geocodeService.geocodeAddress(address + ' ' + cp + ' ' + city)
      .subscribe((location: Location) => {
        if (isCanton) {
          this.cantonCenter = location;
        } else {
          const point: MapPoint = this.cantonData.filter((e) => e.address == address && e.city == city && e.postalCode == cp)[0];
          point.latitude = '' + location.lat;
          point.longitude = '' + location.lng;
          this.firebaseDB.updatePoint(point);
        }
        this.ref.detectChanges();
      }
    );
  }

  getPosition() {
    this.getLocation('Canton of ' + this.cantonSelected, '', '', true);

    if (this.cantonData !== undefined) {
      this.centersFound = true;
      this.cantonData.filter(
        (e: MapPoint) => e.address !== '' && e.postalCode !== '' && e.city !== '' && e.latitude === '' && e.longitude === ''
      ).forEach((e: MapPoint) => {
        this.getLocation(e.address, e.postalCode, e.city, false);
      });
    } else {
      this.centersFound = false;
    }
  }

  async onSelect(value: string) {
    console.log(value);
    this.cantonSelected = value;
    if (this.cantonSelected.match(this.regex) !== null) {
      const abbr = this.cantonSelected.match(this.regex)[0].substring(1, 3);
      this.cantonData = await this.firebaseDB.getCantonData(abbr);
      this.typeMap = {};
      for (const t of this.cantonData) {
        (!(t.type in this.typeMap)) ? this.typeMap[t.type] = [] : null;
        this.typeMap[t.type].push(t);
      }
    }
    this.getPosition();
  }

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {
      console.log(params['canton']); this.onSelect(params['canton']); // (+) converts string 'id' to a number
    });
   }
}
