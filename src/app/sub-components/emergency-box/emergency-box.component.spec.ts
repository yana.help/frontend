import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmergencyBoxComponent } from './emergency-box.component';

describe('EmergencyBoxComponent', () => {
  let component: EmergencyBoxComponent;
  let fixture: ComponentFixture<EmergencyBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmergencyBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmergencyBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
