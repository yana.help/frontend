import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { FirebaseDataService } from '../../services/firebase-data.service';


@Component({
  selector: 'app-cantonbox',
  templateUrl: './cantonbox.component.html',
  styleUrls: ['./cantonbox.component.css']
})
export class CantonboxComponent implements OnInit {

  cantonSelector: string = null;
  queryField: FormControl = new FormControl();
  show: boolean = false;
  results: any[] = [];
  cantons: String[] =this.firebaseDB.getCantons();

  @Output() messageEvent = new EventEmitter<string>();

  constructor(private firebaseDB: FirebaseDataService) { }

  suggest(data : string){
    var newArr = this.cantons.filter(canton => canton.toLowerCase().includes(data.toLowerCase()));
    return newArr;
  }

  change(result){
    this.cantonSelector = result;
    this.queryField.setValue(result);
    this.show = false;
    this.messageEvent.emit(this.cantonSelector)
  }

  /*checks if empty and shows all cantons in that case*/
  send(){
    if(this.queryField.value == null){
      this.results = this.cantons;
      this.show = true;
    }
  }

  ngOnInit(): void {
    this.queryField.valueChanges
    .subscribe(query => this.results = this.suggest(query));
  }

}
