import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommunityComponent } from './community/community.component';
import { ArticleComponent } from './article/article.component';
import { GettingStartedComponent } from './getting-started/getting-started.component';
import { MapsComponent } from './maps/maps.component';
import { ProfessionalContactComponent } from './professional-contact/professional-contact.component';
import { PreventiveMeasuresComponent } from './preventive-measures/preventive-measures.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { MessageComponent } from './message/message.component';


const routes: Routes = [
  {
    path: 'maps',
    component: MapsComponent
  },
  {
    path: 'community',
    component: CommunityComponent
  },
  {
    path: 'community/:id',
    component: CommunityComponent
  },
  {
    path: 'preventive-measures',
    component: PreventiveMeasuresComponent
  },
  {
    path: 'article/:id',
    component: ArticleComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'signup',
    component: SignupComponent
  },
  {
    path: 'maps/:canton',
    component: MapsComponent
  },
  {
    path: '',
    component: GettingStartedComponent
  },
  {
    path: 'messages',
    component: MessageComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
