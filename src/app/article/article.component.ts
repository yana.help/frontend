import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as sampleData from '../../assets/data.json';
import { LangService } from '../services/lang.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit, OnDestroy {

  Texts = (sampleData as any).default;
  selectedText: any;
  id: number;
  private sub: any;
  translate: TranslateService;
  constructor(private route: ActivatedRoute, public lang: LangService) {
    this.translate = lang.getTranslateService();
  }

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
    });
    this.selectedText = this.Texts[this.id];
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

}
